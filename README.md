После получения необходимо создать конфиг yii для базы данных и запустить две миграции:

1. m170118_123629_create_students_table
2. m170118_125018_fill_students_table

при помощи

```
#!batch

yii migrate/to <migration_name>
```