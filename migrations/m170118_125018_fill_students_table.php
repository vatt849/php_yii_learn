<?php

use yii\db\Migration;

class m170118_125018_fill_students_table extends Migration
{
	function getDatetimeNow() {
		$tz_object = new DateTimeZone('Europe/Moscow');

		$datetime = new DateTime();
		$datetime->setTimezone($tz_object);
		return $datetime->format("Y-m-d H:i:s");
	}

	public function up()
	{
		//массив имён
		$names = array(
			1 => 'Ivan', 
			2 => 'Pjotr', 
			3 => 'Sergej', 
			4 => 'Andrej', 
			5 => 'Aleksandr',
			6 => 'Anton',
			7 => 'Nikita',
			8 => 'Ilja',
			9 => 'Klement',
			10 => 'Gregorzh', 
		);

		//массив фамилий
		$surnames = array(
			1 => 'Ivanov', 
			2 => 'Petrov', 
			3 => 'Sidorov', 
			4 => 'Aleksandrov', 
			5 => 'Semjonov', 
			6 => 'Kozlov', 
			7 => 'Sementsov', 
			8 => 'Polezhajkin', 
			9 => 'Voroshilov', 
			10 => 'Brdzhendzhistikevic', 
		);

		//массив групп
		$classes = array(
			1 => '1A', 
			2 => '1B', 
			3 => '2A', 
			4 => '2B', 
			5 => '3A',
		);

		//цикл, в котором всё это дерьмо заносится в таблицу
		for($_r = 1; $_r <= 100; $_r++){
			$date = self::getDatetimeNow();
			$this->insert('students', [
				'name'			=> $names[rand(1, 10)], 
				'lastname'		=> $surnames[rand(1, 10)], 
				'class'			=> $classes[rand(1, 5)], 
				'create_date'	=> $date, 
				'update_date'	=> $date,
			]);
		}
	}

	public function down()
	{
		for($_r = 2; $_r <= 101; $_r++){
			$this->delete('students', ['id' => $_r]);
		}
	}

	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
