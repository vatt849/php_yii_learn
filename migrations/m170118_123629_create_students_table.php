<?php

use yii\db\Migration;

/**
 * Handles the creation of table `students`.
 */
class m170118_123629_create_students_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	function getDatetimeNow() {
		$tz_object = new DateTimeZone('Europe/Moscow');

		$datetime = new DateTime();
		$datetime->setTimezone($tz_object);
		return $datetime->format("Y-m-d H:i:s");
	}

	public function up()
	{
		$this->createTable('students', [
			'id'			=> $this->primaryKey(),
			'name'			=> $this->string(), 
			'lastname'		=> $this->string(), 
			'class'			=> $this->string(), 
			'create_date'	=> $this->dateTime(), 
			'update_date'	=> $this->dateTime(),
		]);

		$date = self::getDatetimeNow();
		$this->insert('students', [
			'name'			=> 'Victor', 
			'lastname'		=> 'Tinus', 
			'class'			=> '1A', 
			'create_date'	=> $date,
			'update_date'	=> $date,
		]);
	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->delete('students', ['id' => 1]);
		$this->dropTable('students');
	}
}
